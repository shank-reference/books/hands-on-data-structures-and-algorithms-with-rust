use crate::IoTDevice;
use std::cell::RefCell;
use std::rc::Rc;

type BareTree = Rc<RefCell<Node>>;
type Tree = Option<BareTree>;

fn color(node: &Tree) -> Color {
    if let Some(ref n) = node {
        n.borrow().color
    } else {
        Color::Black
    }
}

#[derive(Debug, PartialEq, Clone, Copy)]
enum Color {
    Red,
    Black,
}

#[derive(Debug, PartialEq)]
struct Node {
    pub dev: IoTDevice,
    left: Tree,
    right: Tree,
    pub color: Color,
}

impl Node {
    fn new(dev: IoTDevice) -> Tree {
        Some(Rc::new(RefCell::new(Node {
            dev: dev,
            left: None,
            right: None,
            color: Color::Black,
        })))
    }
}

#[derive(Debug)]
pub struct EfficientDeviceRegistry {
    root: Tree,
    pub length: u64,
}

impl EfficientDeviceRegistry {
    pub fn new() -> Self {
        Self {
            root: None,
            length: 0,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::EfficientDeviceRegistry;
    use super::Node;
}
