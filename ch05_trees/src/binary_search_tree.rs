use crate::IoTDevice;
use std::mem;

type Tree = Option<Box<Node>>;

#[derive(Debug, PartialEq)]
struct Node {
    pub dev: IoTDevice,
    left: Tree,
    right: Tree,
}

impl Node {
    pub fn new(dev: IoTDevice) -> Tree {
        Some(Box::new(Node {
            dev: dev,
            left: None,
            right: None,
        }))
    }
}

#[derive(Debug)]
pub struct DeviceRegistry {
    root: Tree,
    pub length: u64,
}

impl DeviceRegistry {
    pub fn new_empty() -> DeviceRegistry {
        DeviceRegistry {
            root: None,
            length: 0,
        }
    }

    pub fn add(&mut self, device: IoTDevice) {
        self.length += 1;
        let root = mem::replace(&mut self.root, None);
        self.root = self.add_rec(root, device)
    }

    fn add_rec(&self, node: Tree, device: IoTDevice) -> Tree {
        match node {
            Some(mut n) => {
                if n.dev.numerical_id < device.numerical_id {
                    n.left = self.add_rec(n.left, device);
                } else {
                    n.right = self.add_rec(n.right, device);
                }
                Some(n)
            }
            _ => Node::new(device),
        }
    }

    pub fn find(&self, numerical_id: u64) -> Option<IoTDevice> {
        self.find_rec(&self.root, numerical_id)
    }

    fn find_rec(&self, node: &Tree, numerical_id: u64) -> Option<IoTDevice> {
        match node {
            Some(n) => {
                if n.dev.numerical_id == numerical_id {
                    Some(n.dev.clone())
                } else if n.dev.numerical_id < numerical_id {
                    self.find_rec(&n.left, numerical_id)
                } else {
                    self.find_rec(&n.right, numerical_id)
                }
            }
            _ => None,
        }
    }

    pub fn walk(&self, callback: impl Fn(&IoTDevice) -> ()) {
        self.walk_in_order(&self.root, &callback);
    }

    fn walk_in_order(&self, node: &Tree, callback: &impl Fn(&IoTDevice) -> ()) {
        if let Some(n) = node {
            self.walk_in_order(&n.left, callback);
            callback(&n.dev);
            self.walk_in_order(&n.right, callback);
        }
    }
}

#[cfg(test)]
mod tests {
    use super::DeviceRegistry;
    use super::IoTDevice;

    fn setup_device_registry() -> DeviceRegistry {
        let mut reg = DeviceRegistry::new_empty();
        let dev0 = IoTDevice::new(0, "address 0", "path 0");
        let dev1 = IoTDevice::new(1, "address 1", "path 1");
        let dev2 = IoTDevice::new(2, "address 2", "path 2");
        reg.add(dev0);
        reg.add(dev1);
        reg.add(dev2);
        reg
    }

    #[test]
    fn test_device_registry_add() {
        let reg = setup_device_registry();
        assert_eq!(reg.length, 3);
    }

    #[test]
    fn test_device_registry_find() {
        let mut reg = DeviceRegistry::new_empty();
        let dev0 = IoTDevice::new(0, "address 0", "path 0");
        let dev1 = IoTDevice::new(1, "address 1", "path 1");
        let dev2 = IoTDevice::new(2, "address 2", "path 2");
        reg.add(dev0.clone());
        reg.add(dev1.clone());
        reg.add(dev2.clone());

        assert_eq!(reg.find(0), Some(dev0));
        assert_eq!(reg.find(1), Some(dev1));
        assert_eq!(reg.find(2), Some(dev2));
        assert_eq!(reg.find(3), None);
    }

    #[test]
    fn test_device_registry_walk() {
        let reg = setup_device_registry();
        dbg!(&reg);
        reg.walk(|dev| println!("{:?}", dev));
    }
}
