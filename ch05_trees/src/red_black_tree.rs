use crate::IotDevice;
use std::cell::RefCell;
use std::rc::Rc;

type BareTree = Rc<RefCell<Node>>;
type Tree = Option<BareTree>;

#[derive(Clone, Debug, PartialEq)]
enum Color{
    Red,
    Black
}

#[derive(PartialEq)]
enum Rotation{
    Left,
    Right
}

struct Node{
    pub color: Color,
    pub dev: IoTDevice,
    pub parent: Tree,
    left: Tree,
    right: Tree
}

impl PartialEq for Node{
    fn eq(&self, other: &Node) -> bool{
        self.dev == other.dev
    }
}

impl Node{
    pub fn new(dev: IoTDevice) -> Tree{
        Some(Rc::new(RefCell::new(Node{
            color: Color::Red,
            dev: dev,
            parent: None,
            left: None,
            right: None
        })))
    }
}

pub struct BetterDeviceRegistry{
    root: Tree,
    pub length: u64
}

impl BetterDeviceRegistry{
    pub fn new_empty() -> BetterDeviceRegistry{
        BetterDeviceRegistry{
            root: None,
            length: 0
        }
    }

    pub fn add(&mut self, device: IoTDevice) {}

    pub fn is_a_valid_red_black_tree(&self) -> bool{

    }   

    pub fn find(&self, numerical_id: u64) -> Option<IoTDevice>{} 

    pub fn walk(&self, callback: impl Fn(&IoTDevice) -> ()) {}
}