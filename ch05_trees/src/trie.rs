use crate::IoTDevice;
use std::collections::HashMap;

type Link = Box<Node>;

#[derive(Debug)]
struct Node {
    key: char,
    value: Option<IoTDevice>,
    next: HashMap<char, Link>,
}

impl Node {
    pub fn new(key: char, device: Option<IoTDevice>) -> Link {
        Box::new(Node {
            key: key,
            value: device,
            next: HashMap::new(),
        })
    }
}

impl PartialEq for Node {
    fn eq(&self, other: &Node) -> bool {
        self.key == other.key
    }
}

#[derive(Debug)]
struct BestDeviceRegistry {
    length: u64,
    root: HashMap<char, Link>,
}

impl BestDeviceRegistry {
    fn new_empty() -> Self {
        Self {
            length: 0,
            root: HashMap::new(),
        }
    }

    pub fn add(&mut self, device: IoTDevice) {
        let path = device.path.clone();
        let mut path_chars = path.chars();
        if let Some(start) = path_chars.next() {
            self.length += 1;
            let mut n = self.root.entry(start).or_insert(Node::new(start, None));
            for c in path_chars {
                n = n.next.entry(c).or_insert(Node::new(c, None));
            }
            n.value = Some(device);
        }
    }

    pub fn find(&self, path: &str) -> Option<IoTDevice> {
        let mut path_chars = path.chars();
        path_chars.next().map_or(None, |start| {
            self.root.get(&start).map_or(None, |mut node| {
                for c in path_chars {
                    match node.next.get(&c) {
                        Some(tmp) => node = tmp,
                        None => break,
                    }
                }
                node.value.clone()
            })
        })
    }

    pub fn walk(&self, callback: impl Fn(&IoTDevice) -> ()) {
        unimplemented!()
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use rand::prelude::*;

    fn new_device_with_id_path(id: u64, path: impl Into<String>) -> IoTDevice {
        IoTDevice::new(id, format!("My address is {}", id), path)
    }

    #[test]
    fn trie_add() {
        let mut trie = BestDeviceRegistry::new_empty();
        let len = 10;

        let mut rng = thread_rng();

        for i in 0..len {
            trie.add(new_device_with_id_path(
                i,
                format!("factory{}/machineA/{}", rng.gen_range(0, len), i),
            ))
        }
        assert_eq!(trie.length, len);
    }

    #[test]
    fn trie_find() {
        let mut trie = BestDeviceRegistry::new_empty();
        let len = 3;

        let mut rng = thread_rng();
        let mut paths = vec![];
        for i in 0..len {
            // let s = format!("factory{}/machineA/{}", rng.gen_range(0, len), i);
            let s = format!("f{}/m/{}", rng.gen_range(0, len), i);
            trie.add(new_device_with_id_path(i, s.clone()));
            paths.push(s);
        }

        assert_eq!(trie.length, len);
        assert_eq!(trie.find("100"), None);
    }
}
