use std::collections::{HashMap, HashSet};

type KeyType = u64;

struct InternetOfThings {
    adjacency_list: Vec<Vec<Edge>>,
    nodes: Vec<KeyType>,
}

impl InternetOfThings {
    fn new() -> Self{
        Self{
            adjacency_list: vec![],
            nodes: vec![]
        }
    }

    fn get_node_index(&self, node: KeyType) -> Option<usize> {
        self.nodes.iter().position(|n| n == &node)
    }

    fn set_edges(&mut self, from: KeyType, edges: Vec<(u32, KeyType)>) {
        let edges: Vec<Edge> = edges.into_iter().map(|(weight, node)| Edge{
            weight, node
        }).collect();
    }
}

#[derive(Debug, Clone)]
struct Edge {
    weight: u32,
    node: usize,
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_set_edges() {
        let mut g = InternetOfThings::new();
    }
}
