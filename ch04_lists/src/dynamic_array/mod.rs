use std::cmp;

type Node = Option<u64>;

#[derive(Debug)]
pub struct TimestampSaver {
    buf: Box<[Node]>,
    cap: usize,
    pub length: usize,
}

impl TimestampSaver {
    fn with_capacity(capacity: usize) -> TimestampSaver {
        TimestampSaver {
            buf: vec![None; capacity].into_boxed_slice(),
            cap: capacity,
            length: 0,
        }
    }

    pub fn push(&mut self, timestamp: u64) {
        if self.length == self.cap {
            self.grow(self.cap);
        }
        self.buf[self.length] = Some(timestamp);
        self.length += 1;
    }

    fn grow(&mut self, min_cap: usize) {
        let old_cap = self.buf.len();
        let mut new_cap = old_cap + (old_cap >> 1);

        new_cap = cmp::max(new_cap, min_cap);
        new_cap = cmp::min(new_cap, usize::max_value());
        let current = self.buf.clone();
        self.cap = new_cap;

        self.buf = vec![None; new_cap].into_boxed_slice();
        self.buf[..current.len()].clone_from_slice(&current);
    }

    pub fn at(&mut self, index: usize) -> Option<u64> {
        if (0..self.length).contains(&index) {
            self.buf[index]
        } else {
            None
        }
    }

    pub fn iter(self) -> ListIterator {
        ListIterator {
            current: 0,
            length: self.length,
            data: self.buf,
        }
    }
}

pub struct ListIterator {
    current: usize,
    length: usize,
    data: Box<[Node]>,
}

impl Iterator for ListIterator {
    type Item = u64;

    fn next(&mut self) -> Option<u64> {
        if (0..self.data.len()).contains(&self.current) {
            let item = self.data[self.current];
            self.current += 1;
            item
        } else {
            None
        }
    }

    // fn rev(self) -> Rev<ListIterator> {
    //     let mut result = Iterator::rev(self);
    //     result.iter.current = self.length - 1;
    //     result
    // }
}

use std::iter::Rev;

impl DoubleEndedIterator for ListIterator {
    fn next_back(&mut self) -> Option<u64> {
        if (0..self.data.len()).contains(&self.current) {
            let item = self.data[self.current];
            if self.current == 0 {
                self.current = self.data.len() - 1;
            } else {
                self.current -= 1;
            }
            item
        } else {
            None
        }
    }
}

// #[test]
// fn test_reverse_iterator() {
//     let mut arr = TimestampSaver::with_capacity(8);
//     arr.push(1);
//     arr.push(2);
//     arr.push(3);

//     let mut iter = arr.iter().rev();
//     assert_eq!(iter.next(), Some(3));
//     assert_eq!(iter.next(), Some(2));
//     assert_eq!(iter.next(), Some(1));
//     assert_eq!(iter.next(), None);
// }

#[test]
fn test_push_and_at() {
    let mut arr = TimestampSaver::with_capacity(8);
    arr.push(1);
    arr.push(2);
    assert_eq!(arr.at(0), Some(1));
    assert_eq!(arr.at(1), Some(2));
    assert_eq!(arr.at(2), None);
}

#[test]
fn test_grow() {
    let mut arr = TimestampSaver::with_capacity(3);
    arr.push(1);
    arr.push(2);
    arr.push(3);

    assert_eq!(arr.length, 3);
    assert_eq!(arr.cap, 3);

    arr.push(4);
    assert_eq!(arr.length, 4);
    assert!(arr.cap > 3);
}

#[test]
fn test_iterator() {
    let mut arr = TimestampSaver::with_capacity(3);
    arr.push(1);
    arr.push(2);
    arr.push(3);
    let mut iter = arr.iter();
    assert_eq!(iter.next(), Some(1));
    assert_eq!(iter.next(), Some(2));
    assert_eq!(iter.next(), Some(3));
    assert_eq!(iter.next(), None);
}
