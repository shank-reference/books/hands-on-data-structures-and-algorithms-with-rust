use rand;
use std::cell::RefCell;
use std::rc::Rc; // 0.7.0

#[derive(Debug, Clone)]
struct Node {
    value: u32,
    next: Vec<Link>,
}

impl Node {
    fn new(value: u32, level: usize) -> Rc<RefCell<Node>> {
        Rc::new(RefCell::new(Node {
            value,
            next: vec![None; level + 1],
        }))
    }
}

type Link = Option<Rc<RefCell<Node>>>;

#[derive(Debug)]
struct AppendOnlySkipList {
    head: Link,
    tails: Vec<Link>,
    length: u64,
    max_level: usize,
}

impl AppendOnlySkipList {
    // levels: [0..n_levels)
    fn new_empty(n_levels: usize) -> AppendOnlySkipList {
        AppendOnlySkipList {
            head: None,
            tails: vec![None; n_levels],
            length: 0,
            max_level: n_levels - 1,
        }
    }

    fn get_level(&self) -> usize {
        let mut level = 0;
        while rand::random::<bool>() && level < self.max_level {
            level += 1;
        }
        level
    }

    fn append(&mut self, value: u32) {
        let level = if self.head.is_none() {
            self.max_level
        } else {
            self.get_level()
        };
        let node = Node::new(value, level);

        if self.head.is_none() {
            self.head = Some(node.clone());
        }

        for i in 0..=level {
            if let Some(tail_i) = self.tails[i].take() {
                let tail_next = &mut tail_i.borrow_mut().next;
                tail_next[i] = Some(node.clone());
            }
            self.tails[i] = Some(node.clone());
        }

        self.length += 1;
    }

    fn find(&self, offset: u32) -> Option<u32> {
        match self.head {
            Some(ref head) => {
                if offset < head.borrow().value
                    || offset > self.tails[0].clone().unwrap().borrow().value
                {
                    return None;
                }

                let mut curr = head.clone();
                for level in (0..=self.max_level).rev() {
                    loop {
                        if curr.borrow().value == offset {
                            return Some(curr.borrow().value);
                        }

                        let next = curr.borrow().next[level].clone();
                        match next {
                            Some(ref next) => {
                                if next.borrow().value > offset {
                                    return None;
                                } else {
                                    curr = next.clone();
                                }
                            }
                            None => break,
                        }
                    }
                }

                None
            }
            None => None,
        }
    }
}

#[test]
fn test_skip_list() {
    let items = [
        (1, "FIRST".to_string()),
        (2, "SECOND".to_string()),
        (3, "THIRD".to_string()),
    ];
    let mut skiplist = AppendOnlySkipList::new_empty(4);

    // dbg!(&skiplist);

    skiplist.append(items[0].0);
    skiplist.append(items[1].0);
    skiplist.append(items[2].0);
    // dbg!(&skiplist);

    assert_eq!(skiplist.find(1), Some(1));
    assert_eq!(skiplist.find(3), Some(3));
    assert_eq!(skiplist.find(0), None);
}
